package Escalonador;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Entrada {
	
	static int maxProcessos = 10;
	static int qntInstrucoes=21;
	int [] priorities = new int[maxProcessos];
	int quantum;
	String processName;
	String[] processInstructions;
	int countP = 1;
	
	public void readPriorities() throws IOException {
		File prioridades = new File("src/Processos/prioridades.txt");
		BufferedReader br = new BufferedReader(new FileReader(prioridades));
		for (int i = 0; i < 10; i++) {
			priorities[i] = Integer.parseInt(br.readLine());
			System.out.println(priorities[i]);
		}
		br.close();
	}
	
	public void readQuantum() throws IOException {
		File quantumFile = new File("src/Processos/quantum.txt");
		BufferedReader br = new BufferedReader(new FileReader(quantumFile));
		quantum = Integer.parseInt(br.readLine());
		System.out.println(quantum);
		br.close();
	}
	
	public void readProcess(File processo) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(processo));
		processName = br.readLine();
		int i=0;
		String aux = null;
		String auxSaida = "SAIDA";
		String[] auxProcess = new String[21];
		
		while(!auxSaida.equals(null) && i<21){
			aux = br.readLine();
			auxProcess[i] = aux;
			i++;
		}
		
		processInstructions = new String[i];
		for (int j = 0; j < i; j++) {
			if (auxProcess[j]!=null) {
				processInstructions[j] = auxProcess[j];
				System.out.println(processInstructions[j]);
			}
		}
		br.close();
//		createProcess();
		
	}
	
	public void readAllFiles() throws IOException{
		String extencao = ".txt";
		String path = "src/Processos/";
		String zero ="0";
		File file;
		for (int i =1; i<=10; i++) {
			if (i!=10) {
				file = new File(path+zero+i+extencao);		
			}
			else {
				file = new File(path+i+extencao);
			}
			System.out.println();
			System.out.println("Processo"+i);
			System.out.println(countP);
			readProcess(file);
			countP++;
		}
	}
	
	public void createProcess() {
		Processo pr = new Processo(priorities[countP], processName, 2, processInstructions);
	}
	
	public void crateBCP() {
		Bcp b = new Bcp(0, 2, processName);
	}
	
	public static void main (String[] args) throws IOException {
		Entrada teste = new Entrada();
//		teste.readQuantum();
//		teste.readPriorities();
//		File processo = new File("src/Processos/01.txt");
//		teste.readProcess(processo);
		teste.readAllFiles();
	}	
}
