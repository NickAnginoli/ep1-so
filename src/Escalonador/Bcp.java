package Escalonador;

//nao mudei os modificadores de acesso, mas criei getters e setter pra se caso resolvermos arrumar isso, por enquanto vou deixar tudo publico
public class Bcp {
	
	int programCounter; //?????
	int estado;
	int regX;
	int regY;
	String nome;
	//falta ref a memoria??
	
	private final int executando = 1;
	private final int pronto = 2;
	private final int bloqueado = 3;
	
	public Bcp (int pc, int e, String n){
		this.programCounter = pc;
		this.estado = e;
		this.nome = n;
	}
	
	public int getProgramCounter() {
		return programCounter;
	}
	public void setProgramCounter(int programCounter) {
		this.programCounter = programCounter;
	}
	public int getEstado() {
		return estado;
	}
	public void setEstado(int estado) {
		this.estado = estado;
	}
	public int getRegX() {
		return regX;
	}
	public void setRegX(int regX) {
		this.regX = regX;
	}
	public int getRegY() {
		return regY;
	}
	public void setRegY(int regY) {
		this.regY = regY;
	}
	public String getNome() {
		return nome;
	}
	
}
