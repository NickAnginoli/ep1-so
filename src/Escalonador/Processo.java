package Escalonador;

//nao mudei os modificadores de acesso, mas criei getters e setter pra se caso resolvermos arrumar isso, por enquanto vou deixar tudo publico
public class Processo {
	int prioridade;
	String nome;
	int estado;
	String[] instrucoes;
	int credito;
	
	public Processo(int prioridade, String nome, int estado, String [] instrucoes) {
		this.prioridade = prioridade;
		this.nome = nome;
		this.estado = estado;
		this.credito = prioridade;
		
		for(int i = 0; i<instrucoes.length; i++) 
			this.instrucoes[i] = instrucoes[i];
	}

	public int getPrioridade() {
		return prioridade;
	}

	public String getNome() {
		return nome;
	}
	
	public int getEstado() {
		return estado;
	}	
}
